import pytest

import minigasket
from minigasket import SourceBase


class Source(minigasket.SourceBase):

    def __init__(self, value=None):
        super().__init__()
        self._value = value

    def trigger(self):
        if self._value is None:
            self.send(['START'])
        else:
            self.send(['START_' + self._value])

    @property
    def name(self):
        return 'Source({})'.format('' if self._value is None else self._value)


class Appender(minigasket.SourceSinkBase):

    def __init__(self, val):
        super().__init__()
        self._value = val

    def receive(self, sender, message) -> None:
        self.send(message + [self._value])

    @property
    def name(self):
        return 'Appender({})'.format(self._value)


class Sink(minigasket.SinkBase):

    def __init__(self):
        self.received = []

    def receive(self, sender, message) -> None:
        sender_name = getattr(sender, 'name', type(sender).__name__)
        self.received.append((sender_name, message))


class MultiSink(minigasket.SourceBase):

    def __init__(self):
        super().__init__()
        self.sink_a = minigasket.SinkProxy(self.receive_a)
        self.sink_b = minigasket.SinkProxy(self.receive_b)

    def receive_a(self, sender, message) -> None:
        self.send(message + ['SINK_A'])

    def receive_b(self, sender, message) -> None:
        self.send(message + ['SINK_B'])


class MultiSource(minigasket.SinkBase):

    name: str = 'MultiSource'

    def __init__(self):
        super().__init__()
        self.source_a = minigasket.SourceProxy(self)
        self.source_b = minigasket.SourceProxy(self)

    def receive(self, sender, message) -> None:
        self.source_a.send(message + ['SOURCE_A'])
        self.source_b.send(message + ['SOURCE_B'])


class MultiSinkMultiSource(object):

    def __init__(self):
        self.sink_a = minigasket.SinkProxy(self.receive_a)
        self.sink_b = minigasket.SinkProxy(self.receive_b)
        self.source_a = minigasket.SourceProxy(self)
        self.source_b = minigasket.SourceProxy(self)

    def receive_a(self, sender, message) -> None:
        self.source_a.send(message + ['SINK_A_TO_SOURCE_A'])
        self.source_b.send(message + ['SINK_A_TO_SOURCE_B'])

    def receive_b(self, sender, message) -> None:
        self.source_a.send(message + ['SINK_B_TO_SOURCE_A'])
        self.source_b.send(message + ['SINK_B_TO_SOURCE_B'])


class Filter(minigasket.FilterBase):

    def predicate(self, sender: SourceBase, message) -> bool:
        return message[-1] == 'T'


class CycleException(Exception):
    pass


class CycleSource(minigasket.SourceBase):

    def trigger(self):
        self.send([])


class CyclePrevent(minigasket.SourceSinkBase):

    def receive(self, sender: SourceBase, message) -> None:
        for ref in message:
            if ref == self:
                raise CycleException
        message.append(self)
        self.send(message)


def test_version():
    assert minigasket.__version__ == '0.2.0'


def test_abstract():
    source = Source()

    with pytest.raises(NotImplementedError):
        minigasket.SinkBase().receive(source, None)

    with pytest.raises(NotImplementedError):
        minigasket.SourceSinkBase().receive(source, None)

    with pytest.raises(NotImplementedError):
        minigasket.FilterBase().predicate(source, None)


def test_basic():
    source = Source()
    sink = Sink()
    source.connect(sink)

    source.trigger()
    assert sink.received == [('Source()', ['START'])]


def test_passthrough():
    source = Source()
    sink = Sink()

    source.connect(minigasket.Passthrough()).connect(sink)

    source.trigger()
    assert sink.received == [('Passthrough', ['START'])]


def test_nested():
    source = Source()
    sink = Sink()
    source.connect(Appender('A').connect(Appender('B').connect(sink)))

    source.trigger()
    assert sink.received == [('Appender(B)', ['START', 'A', 'B'])]


def test_chained():
    source = Source()
    sink = Sink()
    source.connect(Appender('A')).connect(Appender('B')).connect(sink)

    source.trigger()
    assert sink.received == [('Appender(B)', ['START', 'A', 'B'])]


def test_nested_pipes():
    source = Source()
    sink = Sink()
    bc = Appender('B').connect(Appender('C'))
    de = Appender('D').connect(Appender('E'))
    bcde = bc.connect(de)
    pipe = Appender('A').connect(bcde).connect(Appender('F'))
    source.connect(pipe).connect(sink)

    source.trigger()
    assert sink.received == [('Appender(F)', ['START', 'A', 'B', 'C', 'D', 'E', 'F'])]


def test_fork():
    source = Source()
    sink1 = Sink()
    sink2 = Sink()
    knot = Appender('A')
    knot.connect(sink1)
    knot.connect(Appender('B')).connect(sink2)
    source.connect(knot)

    source.trigger()
    assert sink1.received == [('Appender(A)', ['START', 'A'])]
    assert sink2.received == [('Appender(B)', ['START', 'A', 'B'])]


def test_tree():
    source = Source()
    sink = [Sink() for _ in range(6)]
    a = source.connect(Appender('A'))
    a.connect(sink[0])
    b = Appender('B')
    a.connect(b)
    b.connect(sink[1])
    b.connect(Appender('C')).connect(sink[2])
    d = Appender('D')
    d.connect(sink[3])
    a.connect(d).connect(Appender('E')).connect(sink[4])
    fg = Appender('F').connect(Appender('G'))
    d.connect(fg).connect(sink[5])

    source.trigger()
    assert sink[0].received == [('Appender(A)', ['START', 'A'])]
    assert sink[1].received == [('Appender(B)', ['START', 'A', 'B'])]
    assert sink[2].received == [('Appender(C)', ['START', 'A', 'B', 'C'])]
    assert sink[3].received == [('Appender(D)', ['START', 'A', 'D'])]
    assert sink[4].received == [('Appender(E)', ['START', 'A', 'D', 'E'])]
    assert sink[5].received == [('Appender(G)', ['START', 'A', 'D', 'F', 'G'])]


def test_disconnect():
    source = Source()
    sink = Sink()

    a = Appender('A')
    b = Appender('B')

    source >> a >> sink
    source >> b >> sink

    source.trigger()
    assert sink.received == [
        ('Appender(A)', ['START', 'A']),
        ('Appender(B)', ['START', 'B'])
    ]

    source.disconnect()

    sink.received.clear()
    source.trigger()
    assert sink.received == []


def test_disconnect_pipe():
    source = Source()
    sink = Sink()

    sa = source >> Appender('A')
    sa >> Appender('B') >> sink

    source.trigger()
    assert sink.received == [('Appender(B)', ['START', 'A', 'B'])]

    sa.disconnect()

    sink.received.clear()
    source.trigger()
    assert sink.received == []


def test_pipe_exceptions():
    sa = Source() >> Appender('A')
    bs = Appender('B') >> Sink()

    source = Source()
    source >> sa

    with pytest.raises(minigasket.PipeCannotReceiveException):
        source.trigger()

    with pytest.raises(minigasket.PipeCannotSendException):
        bs >> Appender('C')

    with pytest.raises(minigasket.PipeCannotSendException):
        bs.disconnect()

    with pytest.raises(minigasket.PipeCannotSendException):
        bs.send(None)


def test_pipe_send():
    sink = Sink()

    ab = Appender('A') >> Appender('B')
    ab >> Appender('C') >> sink

    ab.send(['START'])
    assert sink.received == [('Appender(C)', ['START', 'C'])]


def test_multi_sink():
    source_1 = Source('1')
    source_2 = Source('2')
    sink = Sink()

    multi_sink = MultiSink()
    multi_sink.connect(Appender('COMMON')).connect(sink)
    source_1.connect(multi_sink.sink_a)
    source_2.connect(multi_sink.sink_b)

    source_1.trigger()
    source_2.trigger()
    assert sink.received == [
        ('Appender(COMMON)', ['START_1', 'SINK_A', 'COMMON']),
        ('Appender(COMMON)', ['START_2', 'SINK_B', 'COMMON'])
    ]


def test_multi_source():
    source = Source()
    sink_1 = Sink()
    sink_2 = Sink()

    multi_source = MultiSource()
    source.connect(Appender('COM')).connect(multi_source)

    multi_source.source_a.connect(Appender('A')).connect(Appender('B')).connect(sink_1)
    knot = multi_source.source_b.connect(Appender('C'))
    knot.connect(sink_1)
    knot.connect(Appender('D')).connect(sink_2)

    source.trigger()
    assert sink_1.received == [
        ('Appender(B)', ['START', 'COM', 'SOURCE_A', 'A', 'B']),
        ('Appender(C)', ['START', 'COM', 'SOURCE_B', 'C'])
    ]
    assert sink_2.received == [
        ('Appender(D)', ['START', 'COM', 'SOURCE_B', 'C', 'D'])
    ]


def test_multi_sink_multi_source():
    source_1 = Source('1')
    source_2 = Source('2')
    sink_1 = Sink()
    sink_2 = Sink()

    mss = MultiSinkMultiSource()
    source_1.connect(Appender('A')).connect(mss.sink_a)
    source_2.connect(mss.sink_b)
    mss.source_a.connect(sink_1)
    mss.source_b.connect(Appender('B')).connect(sink_2)

    source_1.trigger()
    source_2.trigger()

    assert sink_1.received == [
        ('MultiSinkMultiSource', ['START_1', 'A', 'SINK_A_TO_SOURCE_A']),
        ('MultiSinkMultiSource', ['START_2', 'SINK_B_TO_SOURCE_A'])
    ]
    assert sink_2.received == [
        ('Appender(B)', ['START_1', 'A', 'SINK_A_TO_SOURCE_B', 'B']),
        ('Appender(B)', ['START_2', 'SINK_B_TO_SOURCE_B', 'B'])
    ]


def test_split_combine():
    source = Source()
    sink = Sink()

    split = MultiSource()
    mss = MultiSinkMultiSource()
    combine = MultiSink()

    source.connect(Appender('COM')).connect(split)
    split.source_a.connect(mss.sink_a)
    split.source_b.connect(mss.sink_b)
    mss.source_a.connect(combine.sink_a)
    mss.source_b.connect(combine.sink_b)
    combine.connect(Appender('END')).connect(sink)

    source.trigger()
    assert sink.received == [
        ('Appender(END)', ['START', 'COM', 'SOURCE_A', 'SINK_A_TO_SOURCE_A', 'SINK_A', 'END']),
        ('Appender(END)', ['START', 'COM', 'SOURCE_A', 'SINK_A_TO_SOURCE_B', 'SINK_B', 'END']),
        ('Appender(END)', ['START', 'COM', 'SOURCE_B', 'SINK_B_TO_SOURCE_A', 'SINK_A', 'END']),
        ('Appender(END)', ['START', 'COM', 'SOURCE_B', 'SINK_B_TO_SOURCE_B', 'SINK_B', 'END'])
    ]


def test_filter():
    source_pass = Source()
    source_reject = Source()
    sink_all = Sink()
    sink_pass = Sink()
    sink_reject = Sink()

    knot = minigasket.Passthrough()
    source_pass.connect(Appender('T')).connect(knot)
    source_reject.connect(Appender('F')).connect(knot)
    knot.connect(sink_all)

    filt = Filter()
    knot.connect(filt)
    filt.connect(sink_pass)
    filt.rejected.connect(sink_reject)

    source_pass.trigger()
    source_pass.trigger()
    source_reject.trigger()
    source_pass.trigger()
    source_reject.trigger()
    source_reject.trigger()

    assert sink_all.received == [
        ('Passthrough', ['START', 'T']),
        ('Passthrough', ['START', 'T']),
        ('Passthrough', ['START', 'F']),
        ('Passthrough', ['START', 'T']),
        ('Passthrough', ['START', 'F']),
        ('Passthrough', ['START', 'F'])
    ]
    assert sink_pass.received == [
        ('Filter', ['START', 'T']),
        ('Filter', ['START', 'T']),
        ('Filter', ['START', 'T'])
    ]
    assert sink_reject.received == [
        ('Filter', ['START', 'F']),
        ('Filter', ['START', 'F']),
        ('Filter', ['START', 'F'])
    ]


def test_operator_1():
    source = Source()
    sink = Sink()
    bc = Appender('B') >> Appender('C')
    de = Appender('D') >> Appender('E')
    bcde = bc >> de
    abcdef = Appender('A') >> bcde >> Appender('F')
    source >> abcdef >> Appender('G') >> sink

    source.trigger()
    assert sink.received == [('Appender(G)', ['START', 'A', 'B', 'C', 'D', 'E', 'F', 'G'])]


def test_operator_2():
    source = Source('OP')
    sink = [Sink() for _ in range(6)]

    a = source >> Appender('A')
    a >> sink[0]
    b = Appender('B')
    a >> b
    b >> sink[1]
    b >> Appender('C') >> sink[2]
    d = Appender('D')
    d >> sink[3]
    a >> d >> Appender('E') >> sink[4]
    fg = Appender('F') >> Appender('G')
    d >> fg >> sink[5]

    source.trigger()
    assert sink[0].received == [('Appender(A)', ['START_OP', 'A'])]
    assert sink[1].received == [('Appender(B)', ['START_OP', 'A', 'B'])]
    assert sink[2].received == [('Appender(C)', ['START_OP', 'A', 'B', 'C'])]
    assert sink[3].received == [('Appender(D)', ['START_OP', 'A', 'D'])]
    assert sink[4].received == [('Appender(E)', ['START_OP', 'A', 'D', 'E'])]
    assert sink[5].received == [('Appender(G)', ['START_OP', 'A', 'D', 'F', 'G'])]


def test_cycle_prevention():
    source = CycleSource()

    # Self-cycle
    a = CyclePrevent()
    a >> a
    with pytest.raises(CycleException):
        a.send([a])

    # Injected self-cycle
    b = CyclePrevent()
    source >> b >> b
    with pytest.raises(CycleException):
        source.trigger()

    # SinkSource-SinkSource
    source.disconnect()
    c = CyclePrevent()
    d = CyclePrevent()
    source >> c
    c >> d
    d >> c
    with pytest.raises(CycleException):
        source.trigger()

    # SourceSink-Pipe
    e = CyclePrevent()
    fg = CyclePrevent() >> CyclePrevent()
    e >> fg >> e
    with pytest.raises(CycleException):
        e.send([e])

    # Pipe-SinkSource
    source.disconnect()
    hi = CyclePrevent() >> CyclePrevent()
    j = CyclePrevent()
    source >> hi >> j
    j >> hi
    with pytest.raises(CycleException):
        source.trigger()

    # Pipe-Pipe
    source.disconnect()
    kl = CyclePrevent() >> CyclePrevent()
    mn = CyclePrevent() >> CyclePrevent()
    source >> kl
    kl >> mn
    mn >> kl
    with pytest.raises(CycleException):
        source.trigger()
